#!/usr/bin/env python3
#DO ANYTHING YOU WANT WITH THIS!

#The following section asks the user to input a number that will later be converted to ROBUX or USD.
#This also checks to see if the user input a interger. If they input anything else, they will be asked to input a interger and try again.
while True:     
    try:
        global value
        value = int(input("Enter a value: "))
    except ValueError:
        print("Please enter an integer!") 
    currency = input('(R)obux or (U)SD: ')
    if currency.upper() == "R":
            final_result = value * 0.0125
            print(f"{final_result} USD")

    elif currency.upper() == "U":
            final_result = value / 0.0125
            print(f"{final_result} Robux")

    else:
            print("That isn't an option silly")
#At this point the program will keep on looping so that the user can use it as many times as they need.
